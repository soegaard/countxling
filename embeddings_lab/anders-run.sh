#sh create_embeddings_lcurve.sh bible/multi
#python2.7 merge.py > bible/multi/all.vecs

cd ../retrofit
python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l biling/en-all.lex -n 10 -o bible.all.i10.biling.vecs
#python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l biling/en-all.lex -n 20 -o bible.all.i20.biling.vecs
#python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l biling/en-all.lex -n 30 -o bible.all.i30.biling.vecs

python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all.equiv -n 10 -o bible.all.i10.babelnet.vecs
#python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all.equiv -n 20 -o bible.all.i20.babelnet.vecs
#python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all.equiv -n 30 -o bible.all.i30.babelnet.vecs

for lng in es fr ro ; do 
    python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l biling/en-all-except-$lng.lex -n 10 -o bible.all-except-$lng.i10.biling.vecs
#    python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l biling/en-all-except-$lng.lex -n 20 -o bible.all-except-$lng.i20.biling.vecs
#    python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l biling/en-all-except-$lng.lex -n 30 -o bible.all-except-$lng.i30.biling.vecs

    python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all-except-$lng.equiv -n 10 -o bible.all-except-$lng.i10.babelnet.vecs
#    python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all-except-$lng.equiv -n 20 -o bible.all-except-$lng.i20.babelnet.vecs
#    python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all-except-$lng.equiv -n 30 -o bible.all-except-$lng.i30.babelnet.vecs
done
python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all-except-en.equiv -n 10 -o bible.all-except-en.i10.babelnet.vecs
#python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all-except-en.equiv -n 20 -o bible.all-except-en.i20.babelnet.vecs
#python2.7 retrofit.py -i ../embeddings_lab/bible/multi/all.vecs -l ../babelnet/all-except-en.equiv -n 30 -o bible.all-except-en.i30.babelnet.vecs

import glob

files=glob.glob("watchtower/multi-salign/pmi/*.vecs")
for f in files:
    fn=f.split("/")[-1]
    for l in [l.strip().split() for l in open(f).readlines()]:
        print l[0]+"_"+fn[:2], " ".join(l[1:])

from os import listdir
from os.path import join
from collections import Counter
import sys


def main():
    thr = int(sys.argv[1])
    dir_path = sys.argv[2]
    for path in [join(dir_path, f) for f in listdir(dir_path) if f.endswith('.txt')]:
        language_prefix = path.split("/")[-1].split(".")[0]
        if len(language_prefix)==2:
            language_prefix+="x"
        with open(path) as fin:
            lines = [line.strip() for line in fin]
        vocab = read_vocab(lines, thr)
        extract_pairs(vocab, lines, language_prefix)


def extract_pairs(vocab, lines, language_prefix):
    i=0
    for line in lines:
        i+=1
        sentence_id=str(i)
        sentence_str = line
        for word in sentence_str.split(' '):
            if word in vocab:
                print ''.join((language_prefix, '_', word.lower(), ' ', sentence_id))


def read_vocab(lines, thr):
    vocab = Counter()
    for line in lines:
        vocab.update(Counter(line.split(' ')))
    return dict([(token, count) for token, count in vocab.items() if count >= thr])

if __name__ == '__main__':
    main()


In this collection of articles, Seth Godin not only gives many creative insights in what good business is all about, he also shows his love for customers.

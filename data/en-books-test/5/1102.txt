I appreciated the way this book helps staff find the focus of their strengths. It's a great way to help people know what others already know about them. I highly recommend this for a staff retreat.

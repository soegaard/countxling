For an avid reader like myself, it's often tough to know what to read next. In this book, Nancy Pearl helped me get quite a few suggestions.

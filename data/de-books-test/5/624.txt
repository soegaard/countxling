Alle Bücher dieser Reihe sind toll. MEine Tochter lernt viel und "hört" auf das Buch eher, als auf mich....Pädagogisch sehr hilfreich.

Move over Pierce Brosnan and Timothy Dalton. Daniel Craig is the best Bond in 20 years and has resurrected the franchise that was on life support.

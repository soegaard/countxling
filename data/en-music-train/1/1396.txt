Buy this disc if you want to feel like someone stole your favorite toy, tagged it with profane sharpie writing, and then tried to sell it back to you as something new;

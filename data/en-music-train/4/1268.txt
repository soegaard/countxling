This band is a great pick-up, especially if you like bands like Shinedown or Tantric. At times, I wish there was more guitar parts on the album, but its a very solid, safe, and well produced release.

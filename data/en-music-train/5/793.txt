Wow! Great quartets played with real gusto and dynamism by the Prager String Quartet. The sound is incredibly crisp and clear. The performances are impeccable. You won't be disappointed

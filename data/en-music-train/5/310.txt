As a completist Gram fan since 1969, the alternate versions of We'll Sweep Out the Ashes, Streets of Baltimore, and Hickory Wind are truly otherworldly. Well worth the price, in and of themselves

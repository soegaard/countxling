#!/usr/bin/perl -w
# DISCo shared task scoring script: average point difference
# Chris Biemann, 1-2011
# biem@informatik.uni-leipzig.de

# this script is the official scoring script for the average poin difference (APD) score 

# input: system output and test/validation file
if (@ARGV ne 2) {die "Arguments(2): test/validation file, system output";}

# format: 3 tab separate columns each. Columns:
# - language and relation identifier, eg EN_V_OBJ
# - word pair seperated by space, e.g. "share task"
# - human/system judgment score in the range of 0-100. 
# lines with # hashmarks as first character are ignored. 
# The order is not important. 

# Entries missing in the system output are assume to have a score of 50.
# Additinal entries in the system output that are not part of the test/validation file are ignored.


# load text/validation data
%testscores=();
$testitems=0;
open(F,"<$ARGV[0]") || die {"Cannot open file $ARGV[0]\n"};
while($in=<F>) {
	chomp($in);
	if (!($in=~m/^#/)) {
		@a=split(/\t/,$in);
		if (@a eq 3) {
			$testscores{"$a[0]\t$a[1]"}=$a[2];
			$testitems++;
		}
	}	
}
close(F);
print "Loaded $testitems test items\n";

# set default scores for system output
%sysscores=();
foreach $key (keys %testscores) {
   $sysscores{$key}=50;
}

# load system output data
$sysitems=0;
open(F,"<$ARGV[1]") || die {"Cannot open file $ARGV[1]\n"};
while($in=<F>) {
	chomp($in);
	if (!($in=~m/^#/)) {
		@a=split(/\t/,$in);
		if (@a eq 3) {
			if (defined $testscores{"$a[0]\t$a[1]"}) {
				$sysscores{"$a[0]\t$a[1]"}=$a[2];
				$sysitems++;
			}		
		}
	}	
}
close(F);
print "Found $sysitems matching items in system output\n";

#compute scores
$sum=0;
foreach $key (keys %testscores) {
   # print "$key: $sysscores{$key}-$testscores{$key}\n ";
   $difference=$sysscores{$key}-$testscores{$key};
   if ($difference<0) {$difference*=(-1);}
   $sum+=$difference;
}

$apd=$sum/$testitems;
print "Average Point Difference: $apd\n";







#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by Emiliano Guevara.
DISCo Workshop 2011 <disco2011workshop@gmail.com>.
"""

import sys
import getopt
import scipy.stats

help_message = '''
DISCo 2011 shared task scoring script: numerical and coarse judgment correlation.
This script takes 2 files as arguments: the file containing the provided
human annotated scores, and the file containing the scores from the submitted
system.
The script relies on the Scipy libraries for Python: http://www.scipy.org/

File format: 3 tab separate columns each. Columns:
- language and relation identifier, eg EN_V_OBJ
- word pair seperated by space, e.g. "share task"
- human/system judgment score for compositionality: numerical score
  (for the numerical approximation task) or one of the strings
  "low", "medium" or "high" for the average point difference (APD) score

The files must be equally ordered and must contain the same number of items,
the script does not perform any text editing actions on them.

The script performs two correlation tests on the data: Spearman rank-order correlation
(Spearman's rho) and Kendall's rank correlation (Kendall's tau), both using
tie correction.
The script deals with both the numerical and the coarse ratings.
The coarse ratings are transformed from "low/medium/high" into "1/2/3" in order
to carry out the correlation analysis.
Each test produces a correlation coefficient and a 2-tailed p-value.
Coefficients will have values between -1 and +1, with 0 implying no correlation.
Correlations of -1 or +1 imply an exact linear relationship.
'''


class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg




def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			original_scores = sys.argv[1]
			system_scores = sys.argv[2]
		except getopt.error, msg:
			raise Usage(msg)
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, ""
		return 2

	original_scores = open(original_scores, 'r')
	system_scores = open(system_scores, 'r')

	original_scores_clean = []
	for line in original_scores:
		line = line.strip()
		line = line.split("\t")
		#print line[2]
		if line[2] == "high":
			score = 3
		elif line[2] == "medium":
			score = 2
		elif line[2] == "low":
			score = 1
		else:
			score = line[2]
		original_scores_clean.append(float(score))

	system_scores_clean = []
	for line in system_scores:
		line = line.strip()
		line = line.split("\t")
		#print line[2]
		if line[2] == "high":
			score = 3
		elif line[2] == "medium":
			score = 2
		elif line[2] == "low":
			score = 1
		else:
			score = line[2]
		system_scores_clean.append(int(score))

	corr, pval   = scipy.stats.spearmanr(original_scores_clean, system_scores_clean)
	corr2, pval2 = scipy.stats.kendalltau(original_scores_clean, system_scores_clean)

	print "Disco2011 evaluation"
	print "*****************************"
	print "Spearman rho: ", corr
	print "P-value: ", pval
	print "*****************************"
	print "Kendall tau: ", corr2
	print "P-value: ", pval2
	print "*****************************"




if __name__ == "__main__":
	sys.exit(main())

Selten wird ein so alter Film so hervorragend neu herausgebracht. Und während ich „Yellow submarine" früher eher doof und albern fand, kann ich ihn mir inzwischen richtig gut anschauen!

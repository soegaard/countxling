


def main():
    for corpus in ['bible', 'euro10k']:
        for features in ['vulic', 'ppdb', '']:
            run_all_tasks(corpus, features)


def run_all_tasks(corpus, features):
    if features == '':
        features_method = ''
        features = 'sid'
    else:
        features_method = features + '/'
    
    #GOLDEN
    run_task(corpus, 'GOLDEN\\ten\\tfr', features, 'python dice-align-fast.py ../../'+corpus+'/enfr/'+features_method+'dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr')
    run_task(corpus, 'GOLDEN\\tfr\\ten', features, 'python dice-align-fast.py ../../'+corpus+'/enfr/'+features_method+'dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R')
    run_task(corpus, 'GOLDEN\\ten\\tes', features, 'python dice-align-fast.py ../../'+corpus+'/enes/'+features_method+'dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es')
    run_task(corpus, 'GOLDEN\\tes\\ten', features, 'python dice-align-fast.py ../../'+corpus+'/enes/'+features_method+'dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R')
    
    #HANSARD
    run_task(corpus, 'HANSARD\\ten\\tfr', features, 'python dice-align-fast.py ../../'+corpus+'/enfr/'+features_method+'dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f')
    run_task(corpus, 'HANSARD\\tfr\\ten', features, 'python dice-align-fast.py ../../'+corpus+'/enfr/'+features_method+'dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R')
    
    #EPPS
    run_task(corpus, 'EPPS\\ten\\tes', features, 'python dice-align-fast.py ../../'+corpus+'/enes/'+features_method+'dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl')
    run_task(corpus, 'EPPS\\tes\\ten', features, 'python dice-align-fast.py ../../'+corpus+'/enes/'+features_method+'dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R')
    
    #RADA
    run_task(corpus, 'RADA\\ten\\tro', features, 'python dice-align-fast.py ../../'+corpus+'/enro/'+features_method+'dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e')
    run_task(corpus, 'RADA\\tro\\ten', features, 'python dice-align-fast.py ../../'+corpus+'/enro/'+features_method+'dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R')

    #WIKTIONARY
#    run_task(corpus, 'WIKTIONARY\\ten\\tfr', 'python ibm-wiktionary-fast.py ../../'+corpus+'/enfr/'+features_method+'dice/model ../data/en-fr-enwiktionary.txt')
#    run_task(corpus, 'WIKTIONARY\\tfr\\ten', 'python ibm-wiktionary-fast.py ../../'+corpus+'/enfr/'+features_method+'dice/model ../data/en-fr-enwiktionary.txt R')
#    run_task(corpus, 'WIKTIONARY\\ten\\tde', 'python ibm-wiktionary-fast.py ../../'+corpus+'/ende/'+features_method+'dice/model ../data/en-de-enwiktionary.txt')
#    run_task(corpus, 'WIKTIONARY\\tde\\ten', 'python ibm-wiktionary-fast.py ../../'+corpus+'/ende/'+features_method+'dice/model ../data/en-de-enwiktionary.txt R')
#    run_task(corpus, 'WIKTIONARY\\ten\\tes', 'python ibm-wiktionary-fast.py ../../'+corpus+'/enes/'+features_method+'dice/model ../data/en-es-enwiktionary.txt')
#    run_task(corpus, 'WIKTIONARY\\tes\\ten', 'python ibm-wiktionary-fast.py ../../'+corpus+'/enes/'+features_method+'dice/model ../data/en-es-enwiktionary.txt R')
    

def run_task(corpus, task, features, command):
    print "result=`" + command + " | tr -d '[[:space:]]'`"
    print ''.join(('echo -e "', corpus, '\\t', task, '\\t', features, '\\t$result"'))
    



if __name__ == '__main__':
    main()


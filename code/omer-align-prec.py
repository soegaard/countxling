from __future__ import division
import sys,re,codecs
from scipy import stats
import numpy as np
import glob,codecs
from representations.embedding import Embedding
from itertools import groupby


def gb(collection):
    keyfunc = lambda x: x[0]
    groups = groupby(sorted(collection, key=keyfunc), keyfunc)
    return {k: set([v for k_, v in g]) for k, g in groups}

Es = Embedding(sys.argv[1], True)
Et = Embedding(sys.argv[2], True)

poss_aligns=[[int(x) for x in l.strip().split()[:3]] for l in open(sys.argv[3]).readlines()]
sure_aligns=[[int(x) for x in l.strip().split()[:3]] for l in open(sys.argv[3]).readlines() if l.strip().split()[-1]!="P"]

#print(len(poss_aligns))

ssents=[l.strip().split(">")[1].split("<")[0].split() for l in codecs.open(sys.argv[4],'r',"utf8",errors='ignore').readlines()]
tsents=[l.strip().split(">")[1].split("<")[0].split() for l in codecs.open(sys.argv[5],'r',"utf8",errors='ignore').readlines()] #swap 3 and 4

if sys.argv[-1] == 'R':
    poss_aligns = [(sid, twid, swid) for sid, swid, twid in poss_aligns]
    sure_aligns = [(sid, twid, swid) for sid, swid, twid in sure_aligns]
    Es, Et = Et, Es
    ssents, tsents = tsents, ssents

poss_aligns = gb([(sid-1, (swid-1, twid-1)) for sid, swid, twid in poss_aligns])
sure_aligns = gb([(sid-1, (swid-1, twid-1)) for sid, swid, twid in sure_aligns])

tot,prec=0.0,0.0

for sid, (ssent, tsent) in enumerate(zip(ssents, tsents)):
    alignment = set()
    swords = [sword.split("_")[0].lower() for sword in ssent]
    twords = [tword.split("_")[0].lower() for tword in tsent]
    tvecs = [Et.represent(tword) if tword in Et.wi else None for tword in twords]
    for i in range(len(swords)):
        sword=swords[i]
        if sword in Es.wi:
            tot+=1.0
            svec = Es.represent(sword)
            sims = [svec.dot(tvec) if tvec is not None else -1.0 for tvec in tvecs]
#            print(poss_aligns[sid])
            if (i,np.argmax(sims)) in poss_aligns[sid]:
                prec+=1.0
    
print(prec/tot)

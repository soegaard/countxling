for emb in ../../europarl-acl15.40d.allwords.vecs ../embs/fr* ; do
    echo $emb
    python2.7 hansard-align.py $emb ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f
    python2.7 hansard-align-reversed.py $emb ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f
    python2.7 hansard-align.py $emb ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f S
    python2.7 hansard-align-reversed.py $emb ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f S

    done

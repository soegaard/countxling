for emb in ../embs/ro* ; do
    echo $emb
    python2.7 hansard-align.py $emb ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e S
    python2.7 hansard-align-reversed.py $emb ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e S
#    python2.7 hansard-align.py ../embeddings/plain/$emb ../raw_datasets/Romanian-English/answers/test.wa.nonullalign ../raw_datasets/Romanian-English/test/test.r ../raw_datasets/Romanian-English/test/test.e
#    python2.7 hansard-align-reversed.py ../embeddings/plain/$emb ../raw_datasets/Romanian-English/answers/test.wa.nonullalign ../raw_datasets/Romanian-English/test/test.r ../raw_datasets/Romanian-English/test/test.e


    done

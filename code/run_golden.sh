echo "*** es ***"
for emb in ../../europarl-acl15.40d.allwords.vecs ../embs/es* ; do #../embs/all.esi_svd.vecs ; do
    echo $emb
#    python2.7 golden-align.py $emb ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es
#    python2.7 golden-align-reversed.py $emb ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es 
    python2.7 golden-align.py $emb ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es S
    python2.7 golden-align-reversed.py $emb ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es S
done
echo "*** fr ***"
for emb in ../../europarl-acl15.40d.allwords.vecs ../embs/fr* ; do
    echo $emb
#    python2.7 golden-align.py $emb ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr
#    python2.7 golden-align-reversed.py $emb ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr 
    python2.7 golden-align.py $emb ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr S
    python2.7 golden-align-reversed.py $emb ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr S
    done

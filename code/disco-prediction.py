import sys
from scipy import stats
from scipy.spatial.distance import cosine as E#euclidean as E
from sklearn.linear_model import LinearRegression as lin
from sklearn import metrics

X_train=[]
y_train=[]
X_test=[]
y_test=[]
E_test=[]
embs=[(l.strip().split()[0].lower(),l.strip().split()[1:]) for l in open(sys.argv[3]).readlines()]
embs=dict(embs)
train_tabs=[l.strip().split() for l in open(sys.argv[1]).readlines()]
test_tabs=[l.strip().split() for l in open(sys.argv[2]).readlines()]
for t in train_tabs:
    t[1]=t[1].lower()
    t[2]=t[2].lower()
    if t[1] in embs and t[2] in embs:
        w1=[float(x) for x in embs[t[1]]]
        w2=[float(x) for x in embs[t[2]]]
        diff=[abs(w1[i]-w2[i]) for i in range(len(w1))]
        y_train.append(int(t[-1]))
        X_train.append(diff + w1 + w2)

#print t[1], t[2], w1, w2

for t in test_tabs:
    t[1]=t[1].lower()
    t[2]=t[2].lower()
    if t[1] in embs and t[2] in embs:
        w1=[float(x) for x in embs[t[1]]]
        w2=[float(x) for x in embs[t[2]]]
        diff=[abs(w1[i]-w2[i]) for i in range(len(w1))]
        y_test.append(int(t[-1]))
        X_test.append(diff + w1 +w2)
        E_test.append(E(w1,w2))

#print E_test
clf=lin()
clf.fit(X_train,y_train)
#print clf.score(X_test,y_test)
y_hat=clf.predict(X_test)
#print y_test, y_hat
print metrics.mean_absolute_error(y_test,y_hat)
print stats.pearsonr(E_test,y_test)
print stats.spearmanr(E_test,y_test)

from representations.matrix_serializer import load_matrix, load_vocabulary


def read_dice(path, reverse=False):
    if reverse:
        path = path + '.reverse'
        #path = path + '.forward'
    else:
        #path = path + '.reverse'
        path = path + '.forward'
    
    model = load_matrix(path)
    source_vocab = load_vocabulary(path + '.source.vocab')[0]
    target_vocab = load_vocabulary(path + '.target.vocab')[0]
    
    return DiceMatrix(model, source_vocab, target_vocab), source_vocab, target_vocab


class DiceMatrix:

    def __init__(self, model, source_vocab, target_vocab):
        self.model = model
        self.source_vocab = source_vocab
        self.target_vocab = target_vocab
    
    def __getitem__(self, index):
        sword, tword = index
        return self.model[self.source_vocab[sword], self.target_vocab[tword]]


from __future__ import division
import sys,re
from scipy import stats
import numpy as np
import glob
from dice import read_dice
from itertools import groupby


def gb(collection):
    keyfunc = lambda x: x[0]
    groups = groupby(sorted(collection, key=keyfunc), keyfunc)
    return {k: set([v for k_, v in g]) for k, g in groups}

poss_aligns=[[int(x) for x in l.strip().split()[:3]] for l in open(sys.argv[2]).readlines()]
sure_aligns=[[int(x) for x in l.strip().split()[:3]] for l in open(sys.argv[2]).readlines() if l.strip().split()[-1]!="P"]

ssents=[l.strip().split(">")[1].split("<")[0].split() for l in open(sys.argv[3]).readlines()]
tsents=[l.strip().split(">")[1].split("<")[0].split() for l in open(sys.argv[4]).readlines()] #swap 3 and 4

if sys.argv[-1] == 'R':
    poss_aligns = [(sid, twid, swid) for sid, swid, twid in poss_aligns]
    sure_aligns = [(sid, twid, swid) for sid, swid, twid in sure_aligns]
    ssents, tsents = tsents, ssents
    model, vocab_source, vocab_target = read_dice(sys.argv[1], True)
else:
    model, vocab_source, vocab_target = read_dice(sys.argv[1])

poss_aligns = gb([(sid-1, (swid-1, twid-1)) for sid, swid, twid in poss_aligns])
sure_aligns = gb([(sid-1, (swid-1, twid-1)) for sid, swid, twid in sure_aligns])

size_a = 0.0
size_s = 0.0
size_a_and_s = 0.0
size_a_and_p = 0.0
for sid, (ssent, tsent) in enumerate(zip(ssents, tsents)):
    alignment = set()
    twords = [tword.split("_")[0].lower() for tword in tsent]
    for swid, sword in enumerate(ssent):
        sword = sword.split("_")[0].lower()
        if sword in vocab_source:
            sims = [model[(sword, tword)] if tword in vocab_target else -1.0 for tword in twords]
            alignment.add((swid, np.argmax(sims)))
    
    sure = sure_aligns[sid]
    poss = poss_aligns[sid]

    size_a += len(alignment)
    size_s += len(sure)
    size_a_and_s += len(alignment & sure)
    size_a_and_p += len(alignment & poss)

print '{0:.4f}'.format(((size_a_and_s + size_a_and_p) / (size_a + size_s)))

result=`python dice-align-fast.py ../../bible/enfr/vulic/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enfr/vulic/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enes/vulic/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enes/vulic/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enfr/vulic/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enfr/vulic/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enes/vulic/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enes/vulic/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enro/vulic/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enro/vulic/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../bible/enfr/ppdb/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enfr/ppdb/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enes/ppdb/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enes/ppdb/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enfr/ppdb/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enfr/ppdb/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enes/ppdb/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enes/ppdb/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enro/ppdb/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enro/ppdb/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../bible/enfr/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enfr/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enes/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enes/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enfr/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enfr/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enes/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enes/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enro/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tsid\t$result"
result=`python dice-align-fast.py ../../bible/enro/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/vulic/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/vulic/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/vulic/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/vulic/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/vulic/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/vulic/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/vulic/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/vulic/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enro/vulic/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enro/vulic/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tvulic\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/ppdb/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/ppdb/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/ppdb/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/ppdb/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/ppdb/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/ppdb/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/ppdb/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/ppdb/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enro/ppdb/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enro/ppdb/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tppdb\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/dice/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/dice/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enfr/dice/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enes/dice/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enro/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tsid\t$result"
result=`python dice-align-fast.py ../../euro10k/enro/dice/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tsid\t$result"

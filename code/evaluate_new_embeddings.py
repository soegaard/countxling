import subprocess,sys

E=sys.argv[1] # input embeddings
T=sys.argv[2] # target language

#subprocess.call(["python","split_print.py",sys.argv[1],sys.argv[2]])
subprocess.call(("python2.7 omer-align-fast.py "+sys.argv[1]+".en "+sys.argv[1]+"."+sys.argv[2]+" ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final."+sys.argv[2]).split())
subprocess.call(("python2.7 omer-align-fast.py "+sys.argv[1]+".en "+sys.argv[1]+"."+sys.argv[2]+" ../data/golden_collection/en-"+sys.argv[2]+"_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final."+sys.argv[2]+" R").split())
if sys.argv[2]=="es":
    subprocess.call(("python2.7 omer-align-fast.py "+sys.argv[1]+".en "+sys.argv[1]+"."+sys.argv[2]+" ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl").split())
    subprocess.call(("python2.7 omer-align-fast.py "+sys.argv[1]+".en "+sys.argv[1]+"."+sys.argv[2]+" ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R").split())

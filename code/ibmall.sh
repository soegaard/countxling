result=`python ibm-align-fast.py ../../bible/enfr/ibm1/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\t$result"
result=`python ibm-align-fast.py ../../bible/enfr/ibm1/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\t$result"
result=`python ibm-align-fast.py ../../bible/enes/ibm1/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\t$result"
result=`python ibm-align-fast.py ../../bible/enes/ibm1/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\t$result"
result=`python ibm-align-fast.py ../../bible/enfr/ibm1/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\t$result"
result=`python ibm-align-fast.py ../../bible/enfr/ibm1/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\t$result"
result=`python ibm-align-fast.py ../../bible/enes/ibm1/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\t$result"
result=`python ibm-align-fast.py ../../bible/enes/ibm1/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\t$result"
result=`python ibm-align-fast.py ../../bible/enro/ibm1/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\t$result"
result=`python ibm-align-fast.py ../../bible/enro/ibm1/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\t$result"
result=`python ibm-align-fast.py ../../euro10k/enfr/ibm1/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\t$result"
result=`python ibm-align-fast.py ../../euro10k/enfr/ibm1/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\t$result"
result=`python ibm-align-fast.py ../../euro10k/enes/ibm1/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\t$result"
result=`python ibm-align-fast.py ../../euro10k/enes/ibm1/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\t$result"
result=`python ibm-align-fast.py ../../euro10k/enfr/ibm1/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\t$result"
result=`python ibm-align-fast.py ../../euro10k/enfr/ibm1/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\t$result"
result=`python ibm-align-fast.py ../../euro10k/enes/ibm1/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\t$result"
result=`python ibm-align-fast.py ../../euro10k/enes/ibm1/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\t$result"
result=`python ibm-align-fast.py ../../euro10k/enro/ibm1/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\t$result"
result=`python ibm-align-fast.py ../../euro10k/enro/ibm1/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\t$result"
result=`python ibm-align-fast.py ../../eurofull/enfr/ibm1/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tfr\t$result"
result=`python ibm-align-fast.py ../../eurofull/enfr/ibm1/model ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tfr\ten\t$result"
result=`python ibm-align-fast.py ../../eurofull/enes/ibm1/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tes\t$result"
result=`python ibm-align-fast.py ../../eurofull/enes/ibm1/model ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tes\ten\t$result"
result=`python ibm-align-fast.py ../../eurofull/enfr/ibm1/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\ten\tfr\t$result"
result=`python ibm-align-fast.py ../../eurofull/enfr/ibm1/model ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\tfr\ten\t$result"
result=`python ibm-align-fast.py ../../eurofull/enes/ibm1/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\ten\tes\t$result"
result=`python ibm-align-fast.py ../../eurofull/enes/ibm1/model ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\tes\ten\t$result"
result=`python ibm-align-fast.py ../../eurofull/enro/ibm1/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\ten\tro\t$result"
result=`python ibm-align-fast.py ../../eurofull/enro/ibm1/model ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\tro\ten\t$result"

for emb in ../embs/de* ../embs/all.esi_svd.vecs ; do
    echo $emb
    python2.7 disco-prediction.py ../data/disco2011-shared-task-complete-dataset/english/num_scores/DISCo_num_EN_train.tsv ../data/disco2011-shared-task-complete-dataset/german/num_scores/DISCo_num_DE_test.tsv $emb
done

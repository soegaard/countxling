for L in fr ; do
    for emb in ../embs/subsets/$L.omer-europarl.vecs ../embs/subsets/$L.klementiev.vecs ../embs/subsets/$L.chandar.vecs ../embs/subsets/$L.europarl.ii_plain_10000.vecs ; do
	#for emb in ../embs/subsets/fr* ; do # ../../europarl-acl15.40d.allwords.vecs ../embs/fr* ; do
	echo $emb
	python2.7 golden-align.py $emb ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr S
    done
done

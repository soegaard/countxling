import sys,re

FILE=sys.argv[1]
LANG=sys.argv[2]

EN=open(sys.argv[1]+".en",'w')
FR=open(sys.argv[1]+"."+sys.argv[2],'w')

for f in [l.strip().split() for l in open(FILE).readlines() if not re.compile("^\s*$").match(l)]:
    if f[0].split("_")[-1]=="en":
        f[0]=f[0].split("_")[0]
        EN.write(" ".join(f)+"\n")
    if f[0].split("_")[-1]==sys.argv[2]:
        f[0]=f[0].split("_")[0]
        FR.write(" ".join(f)+"\n")
EN.close()
FR.close()
                               

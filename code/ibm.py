from collections import defaultdict


def read_ibm(path, reverse=False):
    if reverse:
        path = path + '.reverse'
        #path = path + '.forward'
    else:
        #path = path + '.reverse'
        path = path + '.forward'

    with open(path) as fin:
        lines = [line.split() for line in fin]
    vocab_source = set([s for c, s, t in lines])
    vocab_target = set([t for c, s, t in lines])
    model = defaultdict(float)
    model.update({(s, t): float(c) for c, s, t in lines})
    
#    if reverse:
#        model = defaultdict(0.0)
#        model.update({(t, s): float(c) for c, s, t in lines})
#        vocab_source, vocab_target = vocab_target, vocab_source
    
    return model, vocab_source, vocab_target


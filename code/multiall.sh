result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/ro0.vecs ../../bible/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/pmi/ro0.vecs ../../bible/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/de0.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/de0.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/es0.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/pmi/en0.vecs ../../bible/multi/pmi/es0.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/ro0.vecs ../../bible/multi/sgns/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/multi/sgns/ro0.vecs ../../bible/multi/sgns/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/fr0.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/de0.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/de0.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/es0.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/multi/sgns/en0.vecs ../../bible/multi/sgns/es0.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/ro0.vecs ../../euro10k/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/pmi/ro0.vecs ../../euro10k/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/de0.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/de0.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/es0.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/pmi/en0.vecs ../../euro10k/multi/pmi/es0.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/ro0.vecs ../../euro10k/multi/sgns/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/multi/sgns/ro0.vecs ../../euro10k/multi/sgns/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/fr0.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/de0.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/de0.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/es0.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/multi/sgns/en0.vecs ../../euro10k/multi/sgns/es0.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tes\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\ten\tes\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/ro0.vecs ../../eurofull/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\ten\tro\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/pmi/ro0.vecs ../../eurofull/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\tro\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tfr\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tfr\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/de0.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tde\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/de0.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tde\ten\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/es0.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tes\tmulti\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/pmi/en0.vecs ../../eurofull/multi/pmi/es0.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tes\ten\tmulti\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tes\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\ten\tes\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\tes\ten\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/ro0.vecs ../../eurofull/multi/sgns/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\ten\tro\tmulti\tsgns\t$result"
result=`python omer-align-fast.py ../../eurofull/multi/sgns/ro0.vecs ../../eurofull/multi/sgns/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\tro\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tfr\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/fr0.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tfr\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/de0.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tde\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/de0.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tde\ten\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/es0.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tes\tmulti\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/multi/sgns/en0.vecs ../../eurofull/multi/sgns/es0.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tes\ten\tmulti\tsgns\t$result"

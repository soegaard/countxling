from __future__ import division
import sys,re
from scipy import stats
from scipy.spatial.distance import euclidean as E
import numpy as np
import glob

#todo: filter for stopwords

E_source=[(l.strip().split()[0],l.strip().split()[1:]) for l in open(sys.argv[1]).readlines() if not re.compile("^\s*$").match(l)]
E_source=dict(E_source)
E_target=[(l.strip().split()[0],l.strip().split()[1:]) for l in open(sys.argv[2]).readlines() if not re.compile("^\s*$").match(l)]
E_target=dict(E_target)

files=glob.glob(sys.argv[3])

def get_pair(words):
    source=""
    target=""
    for w in words:
        if w.split("=")[0]=="insign":
            source=w.split("=")[-1].strip("\"/>")
        if w.split("=")[0]=="outsign":
            target=w.split("=")[-1].strip("\"")
    return (source,target)

tot,score=0,0
for f in files:
    dtag=[get_pair(l.strip().split()) for l in open(f).readlines() if l[:6]=="<align"]
    dtag=dict(dtag)
    for s in dtag:
#        print s, dtag[s]
        if s in E_source and dtag[s] in E_target:
#            print "success:", s, dtag[s]
            tot+=1
            cand=""
            mini=1000000000000000000000
            for t in dtag.itervalues():
                if t in E_target:
                    A=[float(x) for x in E_source[s]]
                    B=[float(x) for x in E_target[t]]
                    dist=E(np.array(A),np.array(B))
                    if dist<mini: 
                        mini=dist
                        cand=t
            if cand==dtag[s]:
                score+=1

print score/tot, "(", score, "/", tot, ")"

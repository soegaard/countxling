for L in de ; do # de 
    for embs in ../embs/subsets/$L.omer-europarl.vecs ../embs/subsets/$L.klementiev.vecs ../embs/subsets/$L.chandar.vecs ../embs/subsets/$L.europarl.ii_plain_10000.vecs ; do
	#for embs in ../embs/subsets/de.omer-europarl.vecs ../embs/subsets/de.klementiev.vecs ../embs/subsets/de.chandar.vecs ../embs/subsets/de.europarl.ii_plain_10000.vecs ; do #../embs/subsets/$L.*.vecs ; do
	echo $embs
	python2.7 nearestneighbors_reverse.py /coastal/embeddings_full_story/bilingual_dictionaries/dictionaries_enwiktionary/derived-word-1mdicts/en-$L-enwiktionary.txt $embs
    done
done

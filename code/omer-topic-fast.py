from __future__ import division
import sys,numpy,glob
from collections import Counter as C
from sklearn import linear_model,metrics
from representations.embedding import Embedding
import numpy as np


en = Embedding(sys.argv[1], True)
fr = Embedding(sys.argv[2], True)

#todo: filter for stopwords
EN_stopwords=[l.strip() for l in open("../english.txt").readlines()]
FR_stopwords=[l.strip() for l in open("../german.txt").readlines()]

DIM = 500

F=2

POS_train=sys.argv[3]
NEG_train=sys.argv[4]
POS_test=sys.argv[5]
NEG_test=sys.argv[6]
#print sys.argv
POS_train_files=glob.glob(POS_train+"/*"*F) # 2 if amazon
POS_test_files=glob.glob(POS_test+"/*"*F)
NEG_train_files=glob.glob(NEG_train+"/*"*F)
NEG_test_files=glob.glob(NEG_test+"/*"*F)
#NEG_test_files=glob.glob(NEG_test+"/*/*")

#print len(POS_train_files),"+",len(NEG_train_files), "train files"
#print len(POS_test_files),"+",len(NEG_test_files), "test files"


def add_examples(files, classification, embedding, stopwords):
    bad = 0
    X = []
    y = []
    for t in files:
        text=C(open(t).read().strip().split())
        ROW = [embedding.represent(word) for word in text if word in en.wi and word not in stopwords]
        if len(ROW) > 0:
            ROW = np.mean(ROW, axis=0)
#        else:
#            ROW = np.zeros(DIM)
            X.append(ROW)
            y.append(classification)
    return X, y


train_X = []
train_y = []

X, y = add_examples(POS_train_files, 1, en, EN_stopwords)
train_X.extend(X)
train_y.extend(y)

X, y = add_examples(NEG_train_files, 0, en, EN_stopwords)
train_X.extend(X)
train_y.extend(y)

train_X = np.vstack(train_X)
train_y = np.array(train_y)


test_X = []
test_y = []

X, y = add_examples(POS_test_files, 1, fr, FR_stopwords)
test_X.extend(X)
test_y.extend(y)

X, y = add_examples(NEG_test_files, 0, fr, FR_stopwords)
test_X.extend(X)
test_y.extend(y)

test_X = np.vstack(test_X)
test_y = np.array(test_y)


clf = linear_model.LogisticRegression()
clf.fit(train_X, train_y)
pred_y = clf.predict(test_X)

default_precision = float(sum(test_y)) / len(test_y)

print '{0:.4f}'.format(metrics.f1_score(test_y, pred_y))
#print '|'.join(('{0:.4f}'.format(metrics.accuracy_score(test_y, pred_y)), '{0:.4f}'.format(metrics.precision_score(test_y, pred_y)), '{0:.4f}'.format(metrics.recall_score(test_y, pred_y)), '{0:.4f}'.format(metrics.f1_score(test_y, pred_y)), '{0:.4f}'.format(max(2 * default_precision / (1 + default_precision), 2*(1 - default_precision) / (2 - default_precision)))))
#print metrics.accuracy_score(test_y, pred_y), metrics.f1_score(test_y, pred_y)


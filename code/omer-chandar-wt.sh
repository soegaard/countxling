result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/chandar/binary ../data/golden_collection/en-pt_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.pt | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\ten\tpt\tchandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/pt0.vecs ../data/golden_collection/en-pt_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.pt R | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\tpt\ten\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\ten\tfr\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\tfr\ten\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\ten\tes\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\tes\ten\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "wt\tHANSARD\ten\tfr\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "wt\tHANSARD\tfr\ten\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "wt\tEPPS\ten\tes\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "wt\tEPPS\tes\ten\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/ro0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "wt\tRADA\ten\tro\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/ro0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "wt\tRADA\tro\ten\tsid-chandar\tmulti\t$result"
#result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/tu0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/turkish/alignments.std ../data/turkish/turkish.std ../data/turkish/english.std | tr -d '[[:space:]]'`
#echo -e "wt\tITU\ttu\ten\tsid-chandar\tmulti\t$result"
#result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/tu0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/turkish/alignments.std ../data/turkish/turkish.std ../data/turkish/english.std R | tr -d '[[:space:]]'`
#echo -e "wt\tITU\ten\ttu\tsid-chandar\tmulti\t$result"

result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/sv0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/ep-ensv-alignref.v2015-10-12/test/test.ensv.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.sv.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.en.naacl | tr -d '[[:space:]]'`
echo -e "wt\tEPSV\tsv\ten\tsid-chandar\tmulti\t$result"
result=`python omer-align-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/sv0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/ep-ensv-alignref.v2015-10-12/test/test.ensv.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.sv.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.en.naacl R | tr -d '[[:space:]]'`
echo -e "wt\tEPSV\ten\tsv\tsid-chandar\tmulti\t$result"

result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/ar0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/en-arb-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "wt\tWIKTIONARY\tar\ten\tsid-chandar\tmulti\t$result"
#result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/fa0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/en-fa-enwiktionary.txt | tr -d '[[:space:]]'`
#echo -e "wt\tWIKTIONARY\tfa\ten\tsid-chandar\tmulti\t$result"
result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/fi0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/en-fi-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "wt\tWIKTIONARY\tfi\ten\tsid-chandar\tmulti\t$result"
#result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/binary ../embeddings_lab/watchtower/multi-salign/pmi/fr0.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
#echo -e "wt\tWIKTIONARY\ten\tfr\tsid-chandar\tmulti\t$result"
result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/he0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/en-he-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "wt\tWIKTIONARY\the\ten\tsid-chandar\tmulti\t$result"
result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/hi0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/en-hi-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "wt\tWIKTIONARY\thi\ten\tsid-chandar\tmulti\t$result"
result=`python omer-wiktionary-fast.py ../embeddings_lab/watchtower/multi-salign/chandar/hu0.vecs ../embeddings_lab/watchtower/multi-salign/pmi/binary ../data/en-hu-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "wt\tWIKTIONARY\thu\ten\tsid-chandar\tmulti\t$result"





def main():
    for corpus in ['bible', 'euro10k']:
        for features in ['vulic', 'ppdb', '']:
            for method in ['tfidf', 'pmi', 'sgns']:
                run_all_tasks(corpus, features, method)
        features = None
        method = 'bilbowa'
        run_all_tasks(corpus, features, method)


def run_all_tasks(corpus, features, method):
    if features is None:
        features_method = method
    else:
        features_method = features + '/' + method

    if features is None:
        features = 'N/A'
    elif features == '':
        features = 'sid'
    
    #GOLDEN
    run_task(corpus, 'GOLDEN\\ten\\tfr', features, method, 'python omer-align-fast.py ../../'+corpus+'/enfr/'+features_method+'/eng.vecs ../../'+corpus+'/enfr/'+features_method+'/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr')
    run_task(corpus, 'GOLDEN\\tfr\\ten', features, method, 'python omer-align-fast.py ../../'+corpus+'/enfr/'+features_method+'/eng.vecs ../../'+corpus+'/enfr/'+features_method+'/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R')
    run_task(corpus, 'GOLDEN\\ten\\tes', features, method, 'python omer-align-fast.py ../../'+corpus+'/enes/'+features_method+'/eng.vecs ../../'+corpus+'/enes/'+features_method+'/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es')
    run_task(corpus, 'GOLDEN\\tes\\ten', features, method, 'python omer-align-fast.py ../../'+corpus+'/enes/'+features_method+'/eng.vecs ../../'+corpus+'/enes/'+features_method+'/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R')
    
    #HANSARD
    run_task(corpus, 'HANSARD\\ten\\tfr', features, method, 'python omer-align-fast.py ../../'+corpus+'/enfr/'+features_method+'/eng.vecs ../../'+corpus+'/enfr/'+features_method+'/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f')
    run_task(corpus, 'HANSARD\\tfr\\ten', features, method, 'python omer-align-fast.py ../../'+corpus+'/enfr/'+features_method+'/eng.vecs ../../'+corpus+'/enfr/'+features_method+'/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R')
    
    #EPPS
    run_task(corpus, 'EPPS\\ten\\tes', features, method, 'python omer-align-fast.py ../../'+corpus+'/enes/'+features_method+'/eng.vecs ../../'+corpus+'/enes/'+features_method+'/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl')
    run_task(corpus, 'EPPS\\tes\\ten', features, method, 'python omer-align-fast.py ../../'+corpus+'/enes/'+features_method+'/eng.vecs ../../'+corpus+'/enes/'+features_method+'/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R')
    
    #RADA
    run_task(corpus, 'RADA\\ten\\tro', features, method, 'python omer-align-fast.py ../../'+corpus+'/enro/'+features_method+'/rom.vecs ../../'+corpus+'/enro/'+features_method+'/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R')
    run_task(corpus, 'RADA\\tro\\ten', features, method, 'python omer-align-fast.py ../../'+corpus+'/enro/'+features_method+'/rom.vecs ../../'+corpus+'/enro/'+features_method+'/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e')

    #WIKTIONARY
    run_task(corpus, 'WIKTIONARY\\ten\\tfr', features, method, 'python omer-wiktionary-fast.py ../../'+corpus+'/enfr/'+features_method+'/eng.vecs ../../'+corpus+'/enfr/'+features_method+'/fre.vecs ../data/en-fr-enwiktionary.txt')
    run_task(corpus, 'WIKTIONARY\\tfr\\ten', features, method, 'python omer-wiktionary-fast.py ../../'+corpus+'/enfr/'+features_method+'/eng.vecs ../../'+corpus+'/enfr/'+features_method+'/fre.vecs ../data/en-fr-enwiktionary.txt R')
    run_task(corpus, 'WIKTIONARY\\ten\\tde', features, method, 'python omer-wiktionary-fast.py ../../'+corpus+'/ende/'+features_method+'/eng.vecs ../../'+corpus+'/ende/'+features_method+'/deu.vecs ../data/en-de-enwiktionary.txt')
    run_task(corpus, 'WIKTIONARY\\tde\\ten', features, method, 'python omer-wiktionary-fast.py ../../'+corpus+'/ende/'+features_method+'/eng.vecs ../../'+corpus+'/ende/'+features_method+'/deu.vecs ../data/en-de-enwiktionary.txt R')
    run_task(corpus, 'WIKTIONARY\\ten\\tes', features, method, 'python omer-wiktionary-fast.py ../../'+corpus+'/enes/'+features_method+'/eng.vecs ../../'+corpus+'/enes/'+features_method+'/spa.vecs ../data/en-es-enwiktionary.txt')
    run_task(corpus, 'WIKTIONARY\\tes\\ten', features, method, 'python omer-wiktionary-fast.py ../../'+corpus+'/enes/'+features_method+'/eng.vecs ../../'+corpus+'/enes/'+features_method+'/spa.vecs ../data/en-es-enwiktionary.txt R')
    
    #AMAZON
    for t1, t2 in [('books', 'dvd'), ('books', 'music'), ('dvd', 'books'), ('dvd', 'music'), ('music', 'books'), ('music', 'dvd')]:
        run_task(corpus, 'AMAZON_'+t1+'-'+t2+'\\ten\\tde', features, method, 'python omer-topic-fast.py ../../'+corpus+'/ende/'+features_method+'/eng.vecs ../../'+corpus+'/ende/'+features_method+'/deu.vecs ../data/en-'+t1+'-train/ ../data/en-'+t2+'-train/ ../data/de-'+t1+'-test/ ../data/de-'+t2+'-test/')


def run_task(corpus, task, features, method, command):
    print "result=`" + command + " | tr -d '[[:space:]]'`"
    print ''.join(('echo -e "', corpus, '\\t', task, '\\t', features, '\\t', method, '\\t$result"'))
    



if __name__ == '__main__':
    main()


result=`python omer-align-fast.py ../../eurofull/enfr//pmi/eng.vecs ../../eurofull/enfr//pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tfr\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enfr//pmi/eng.vecs ../../eurofull/enfr//pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tfr\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enes//pmi/eng.vecs ../../eurofull/enes//pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\ten\tes\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enes//pmi/eng.vecs ../../eurofull/enes//pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "eurofull\tGOLDEN\tes\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enfr//pmi/eng.vecs ../../eurofull/enfr//pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\ten\tfr\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enfr//pmi/eng.vecs ../../eurofull/enfr//pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "eurofull\tHANSARD\tfr\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enes//pmi/eng.vecs ../../eurofull/enes//pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\ten\tes\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enes//pmi/eng.vecs ../../eurofull/enes//pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "eurofull\tEPPS\tes\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enro//pmi/rom.vecs ../../eurofull/enro//pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\ten\tro\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../eurofull/enro//pmi/rom.vecs ../../eurofull/enro//pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "eurofull\tRADA\tro\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/enfr//pmi/eng.vecs ../../eurofull/enfr//pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tfr\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/enfr//pmi/eng.vecs ../../eurofull/enfr//pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tfr\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tde\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tde\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/enes//pmi/eng.vecs ../../eurofull/enes//pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\ten\tes\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../eurofull/enes//pmi/eng.vecs ../../eurofull/enes//pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "eurofull\tWIKTIONARY\tes\ten\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "eurofull\tAMAZON_books-dvd\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "eurofull\tAMAZON_books-music\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "eurofull\tAMAZON_dvd-books\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "eurofull\tAMAZON_dvd-music\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "eurofull\tAMAZON_music-books\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../eurofull/ende//pmi/eng.vecs ../../eurofull/ende//pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "eurofull\tAMAZON_music-dvd\ten\tde\tsid\tpmi\t$result"

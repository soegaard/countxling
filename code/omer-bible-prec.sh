result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/pt0.vecs ../data/golden_collection/en-pt_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.pt | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\ten\tpt\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/pt0.vecs ../data/golden_collection/en-pt_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.pt R | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\tpt\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\ten\tfr\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/fr0.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\tfr\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\ten\tes\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/es0.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "wt\tGOLDEN\tes\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "wt\tHANSARD\ten\tfr\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/fr0.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "wt\tHANSARD\tfr\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "wt\tEPPS\ten\tes\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/es0.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "wt\tEPPS\tes\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/ro0.vecs ../embeddings_lab/bible/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "wt\tRADA\ten\tro\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/ro0.vecs ../embeddings_lab/bible/multi/pmi/en0.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "wt\tRADA\tro\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/tr0.vecs ../data/turkish/alignment.std ../data/turkish/english.std ../data/turkish/turkish.std | tr -d '[[:space:]]'`
echo -e "wt\tITU\ten\ttu\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/tr0.vecs ../data/turkish/alignment.std ../data/turkish/english.std ../data/turkish/turkish.std R | tr -d '[[:space:]]'`
echo -e "wt\tITU\ttu\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/sv0.vecs ../data/ep-ensv-alignref.v2015-10-12/test/test.ensv.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.en.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.sv.naacl | tr -d '[[:space:]]'`
echo -e "wt\tEPSV\tsv\ten\tsid-pmi\tmulti\t$result"
result=`python omer-align-prec.py ../embeddings_lab/bible/multi/pmi/en0.vecs ../embeddings_lab/bible/multi/pmi/sv0.vecs ../data/ep-ensv-alignref.v2015-10-12/test/test.ensv.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.en.naacl ../data/ep-ensv-alignref.v2015-10-12/test/test.sv.naacl R | tr -d '[[:space:]]'`
echo -e "wt\tEPSV\ten\tsv\tsid-pmi\tmulti\t$result"

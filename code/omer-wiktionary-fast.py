from __future__ import division
import sys,codecs
import numpy as np
from representations.embedding import Embedding


Es = Embedding(sys.argv[1], True)
Et = Embedding(sys.argv[2], True)

BX=[(l.split("|||")[0].strip(),l.split("|||")[-1].strip()) for l in codecs.open(sys.argv[3],'r','utf8').readlines()]
BD=[]
for s,t in BX:
    #print(s,t)
    if s in Es.wi and t in Et.wi:
        BD.append((s,t))

if sys.argv[-1] == 'R':
    Es, Et = Et, Es
    BD = [(t, s) for s, t in BD]

p1,tot=0,0
for s, t in BD:
    vs = Es.represent(s)
    scores = vs.dot(Et.m.T)
    cand = Et.iw[np.nanargmax(scores)]
    if t==cand:
        p1+=1
    tot+=1

print(p1/tot)
#print p1/tot, "(P@1)", p1, "/", tot

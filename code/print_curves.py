import sys,pylab,glob

files=glob.glob("exp.*")
print files
for f in files:
    x,y=[],[]
    lines=[l.strip().split() for l in open(f).readlines()]

    results={}
    for l in lines:
        if len(l)==1:
            F=".".join(l[0].split("/")[-1].split(".")[1:])
        else:
            r=l[0]
            results[F]=r

    compare=["europarl.ii_plain_10000.vecs",
             "europarl.ii_plain_20000.vecs",
             "europarl.ii_plain_30000.vecs",
             "europarl.ii_plain_40000.vecs",
             "europarl.ii_plain_50000.vecs",
             "europarl.ii_plain_100000.vecs",
             "europarl.ii_plain_150000.vecs"]
    for c in compare:
        if c in results:
            x.append(int(c.split(".")[-2].split("_")[-1]))
            y.append(results[c])
    print f, x, y
    pylab.plot(x,y,label=f.split(".")[-1])

pylab.legend(loc="best")
pylab.style.use('bmh') #fivethirtyeight')
pylab.savefig('curves.png')

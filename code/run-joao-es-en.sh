for L in es ; do
    for emb in ../embs/subsets/$L.omer-europarl.vecs ../embs/subsets/$L.klementiev.vecs ../embs/subsets/$L.chandar.vecs ../embs/subsets/$L.europarl.ii_plain_10000.vecs ; do
	echo $emb
	python2.7 golden-align-reversed.py $emb ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es S
    done
done

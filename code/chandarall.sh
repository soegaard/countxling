result=`python omer-align-fast.py ../../bible/enfr//chandar/eng.vecs ../../bible/enfr//chandar/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enfr//chandar/eng.vecs ../../bible/enfr//chandar/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enes//chandar/eng.vecs ../../bible/enes//chandar/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enes//chandar/eng.vecs ../../bible/enes//chandar/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enfr//chandar/eng.vecs ../../bible/enfr//chandar/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enfr//chandar/eng.vecs ../../bible/enfr//chandar/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enes//chandar/eng.vecs ../../bible/enes//chandar/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enes//chandar/eng.vecs ../../bible/enes//chandar/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enro//chandar/rom.vecs ../../bible/enro//chandar/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../bible/enro//chandar/rom.vecs ../../bible/enro//chandar/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//chandar/eng.vecs ../../euro10k/enfr//chandar/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//chandar/eng.vecs ../../euro10k/enfr//chandar/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//chandar/eng.vecs ../../euro10k/enes//chandar/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//chandar/eng.vecs ../../euro10k/enes//chandar/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//chandar/eng.vecs ../../euro10k/enfr//chandar/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//chandar/eng.vecs ../../euro10k/enfr//chandar/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//chandar/eng.vecs ../../euro10k/enes//chandar/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//chandar/eng.vecs ../../euro10k/enes//chandar/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//chandar/rom.vecs ../../euro10k/enro//chandar/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tsid\tchandar\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//chandar/rom.vecs ../../euro10k/enro//chandar/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tsid\tchandar\t$result"

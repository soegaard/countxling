#from gensim.models import Word2Vec
from __future__ import division
import sys,codecs
from scipy.spatial.distance import cosine as D

E_t=dict([(l.strip().split()[0].split("_")[0].lower(),[float(x) for x in l.strip().split()[1:]]) for l in codecs.open(sys.argv[2],'r','utf-8').readlines() if l.strip().split()[0].split("_")[-1]==sys.argv[2].split("/")[-1].split(".")[0] or l.strip().split()[0].split("_")[-1]=="f"])
E_s=dict([(l.strip().split()[0].split("_")[0].lower(),[float(x) for x in l.strip().split()[1:]]) for l in codecs.open(sys.argv[2],'r','utf-8').readlines() if l.strip().split()[0].split("_")[-1]=="en" or l.strip().split()[0].split("_")[-1]=="e"])
BD=[[(w.split("_")[0].lower()) for w in l.strip().split()[:2]] for l in codecs.open(sys.argv[1],'r','utf-8').readlines() if l.strip().split()[0].lower() in E_s and l.strip().split()[1].lower() in E_t]#[:N]

#print sys.argv[2].split("/")[-1].split(".")[0]
#print len(E_t), len(E_s), len(BD)

p1,tot=0,0
for S,T in BD:
    if S in E_s and T in E_t:
        cand="None"
        near=10**10
        for U in E_t:
            dist=D(E_s[S],E_t[U])
            if dist<near:
                near=dist
                cand=U
        #print S, T, cand
        if T==cand:
            p1+=1
        tot+=1

print p1/tot, "(P@1)", p1, "/", tot
#model=Word2Vec.load_word2vec_format(sys.argv[1], binary=False)

#for _ in range(10):
#    word=raw_input("Target:")
#    print model.most_similar(word)

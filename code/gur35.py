from __future__ import division
import sys,re
from scipy import stats
from scipy.spatial.distance import cosine as Ed 
#from scipy.spatial.distance import euclidean as Ed
import numpy as np
import glob

#todo: filter for stopwords

M=[(l.strip().split()[0].split("_")[0].lower(),l.strip().split()[1:]) for l in open(sys.argv[1]).readlines() if not re.compile("^\s*$").match(l)] # merged embeddings file
M=dict(M)

len(M)

X,Y=[],[]
#print sys.argv
#print open(sys.argv[2]).readlines()
WS=[l.strip().split(';') for l in open(sys.argv[2]).read().split('\r')[1:]]
#print WS
for ws in WS:
    W1=ws[0].lower().strip()
    W2=ws[1].lower().strip()
    #print W1, W2
    anns=[float(x.replace(",",".")) for x in [ws[2]] if not re.compile("^\s*$").match(x)]
    if W1 in M and W2 in M:
        A=[float(x) for x in M[W1]]
        B=[float(x) for x in M[W2]]
        X.append(sum(anns))#/len(anns))
        Y.append(Ed(np.array(A),np.array(B)))

#print X,Y
print stats.spearmanr(X,Y)
print stats.pearsonr(X,Y)

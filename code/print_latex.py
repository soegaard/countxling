import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

results={}
for l in lines:
    if len(l)==1:
        f=".".join(l[0].split("/")[-1].split(".")[1:])
    else:
        r=l[0]
        results[f]=r

compare=["klementiev.vecs","chandar.vecs","gouws.vecs","soegaard.vecs","europarl.ii_plain_100000.vecs","europarl.ii_l1_10000.vecs"]
for c in compare:
    print c,"&",
print "\\\\"
for c in compare:
    if c in results:
        print("%.2f &" % float(results[c])),
#        print "%.2f &", %(results[c])
    else:
        print "- &",
print "\\\\"

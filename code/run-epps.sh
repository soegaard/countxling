for emb in ~/word2vec_sampler/en-es.europarl.model.txt ../embs/es* ../embs/all.esi_svd.vecs ; do
    subsets="subsets/"
    subsets=""
    echo $emb
#    python2.7 hansard-align.py $emb ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../raw_datasets/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../raw_datasets/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl
#    python2.7 hansard-align-reversed.py ../embeddings/plain/$subsets/$emb ../raw_datasets/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../raw_datasets/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../raw_datasets/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl
    python2.7 hansard-align.py $emb ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl S
    python2.7 hansard-align-reversed.py $emb ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl S

done

from __future__ import division
import sys,numpy,glob
from collections import Counter as C
from sklearn import linear_model,metrics

DIM=40

EN_stopwords=[l.strip() for l in open("../english").readlines()]
FR_stopwords=[l.strip() for l in open("../german").readlines()]

embs=[[l.strip().split()[0].split("_")[0].lower(),[float(X) for X in l.strip().split()[1:]]] for l in open(sys.argv[3]).readlines()]
embs=dict(embs)

F=2

POS_train=sys.argv[1]
POS_test=sys.argv[2]
#print sys.argv
POS_train_files=glob.glob(POS_train+"/*"*F) # 2 if amazon
POS_test_files=glob.glob(POS_test+"/*"*F)
NEG_train=sys.argv[4]
NEG_test=sys.argv[5]
NEG_train_files=glob.glob(NEG_train+"/*"*F)
NEG_test_files=glob.glob(NEG_test+"/*"*F)
#NEG_test_files=glob.glob(NEG_test+"/*/*")

#print len(POS_train_files),"+",len(NEG_train_files), "train files"
#print len(POS_test_files),"+",len(NEG_test_files), "test files"

cover,wordc=0,0

X=[]
y=[]
for t in POS_train_files:
    ROW=[]
    text=C(open(t).read().strip().split())
    for word in text:
        if word in embs and word not in EN_stopwords:
            ROW.append(embs[word])
    ROW=numpy.array(ROW[:DIM])
    if len(ROW)>0:
        ROW=numpy.mean(ROW, axis=0)
        X.append(ROW)
        y.append(1)
for t in NEG_train_files:
    ROW=[]
    text=C(open(t).read().strip().split())
    for word in text:
        if word in embs and word not in EN_stopwords:
            ROW.append(embs[word])
    ROW=numpy.array(ROW[:DIM])
    if len(ROW)>0:
        ROW=numpy.mean(ROW, axis=0)
        X.append(ROW)
        y.append(0)

X_test=[]
y_test=[]
for t in POS_test_files:
    ROW=[]
    text=C(open(t).read().strip().split())
    for word in text:
        if word in embs and word not in FR_stopwords:
            ROW.append(embs[word])
            cover+=1
        if word not in FR_stopwords:
            wordc+=1
    ROW=numpy.array(ROW[:DIM])
    if len(ROW)>0:
        ROW=numpy.mean(ROW, axis=0)
        X_test.append(ROW)
        y_test.append(1)
for t in NEG_test_files:
    ROW=[]
    text=C(open(t).read().strip().split())
    for word in text:
        if word in embs and word not in FR_stopwords:
            ROW.append(embs[word])
            cover+=1
        if word not in FR_stopwords:
            wordc+=1
    ROW=numpy.array(ROW[:DIM])
    if len(ROW)>0:
        ROW=numpy.mean(ROW, axis=0)
        X_test.append(ROW)
        y_test.append(0)


clf=linear_model.LogisticRegression()
clf.fit(X,y)
y_pred=clf.predict(X_test)
print metrics.f1_score(y_test,y_pred), cover/wordc

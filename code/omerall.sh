result=`python omer-align-fast.py ../../bible/enfr/vulic/tfidf/eng.vecs ../../bible/enfr/vulic/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/tfidf/eng.vecs ../../bible/enfr/vulic/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/tfidf/eng.vecs ../../bible/enes/vulic/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/tfidf/eng.vecs ../../bible/enes/vulic/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/tfidf/eng.vecs ../../bible/enfr/vulic/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/tfidf/eng.vecs ../../bible/enfr/vulic/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/tfidf/eng.vecs ../../bible/enes/vulic/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/tfidf/eng.vecs ../../bible/enes/vulic/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enro/vulic/tfidf/rom.vecs ../../bible/enro/vulic/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enro/vulic/tfidf/rom.vecs ../../bible/enro/vulic/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/vulic/tfidf/eng.vecs ../../bible/enfr/vulic/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/vulic/tfidf/eng.vecs ../../bible/enfr/vulic/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/vulic/tfidf/eng.vecs ../../bible/enes/vulic/tfidf/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/vulic/tfidf/eng.vecs ../../bible/enes/vulic/tfidf/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/tfidf/eng.vecs ../../bible/ende/vulic/tfidf/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/pmi/eng.vecs ../../bible/enfr/vulic/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/pmi/eng.vecs ../../bible/enfr/vulic/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/pmi/eng.vecs ../../bible/enes/vulic/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/pmi/eng.vecs ../../bible/enes/vulic/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/pmi/eng.vecs ../../bible/enfr/vulic/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/pmi/eng.vecs ../../bible/enfr/vulic/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/pmi/eng.vecs ../../bible/enes/vulic/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/pmi/eng.vecs ../../bible/enes/vulic/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enro/vulic/pmi/rom.vecs ../../bible/enro/vulic/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enro/vulic/pmi/rom.vecs ../../bible/enro/vulic/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/vulic/pmi/eng.vecs ../../bible/enfr/vulic/pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/vulic/pmi/eng.vecs ../../bible/enfr/vulic/pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/vulic/pmi/eng.vecs ../../bible/enes/vulic/pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/vulic/pmi/eng.vecs ../../bible/enes/vulic/pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/pmi/eng.vecs ../../bible/ende/vulic/pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/sgns/eng.vecs ../../bible/enfr/vulic/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/sgns/eng.vecs ../../bible/enfr/vulic/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/sgns/eng.vecs ../../bible/enes/vulic/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/sgns/eng.vecs ../../bible/enes/vulic/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/sgns/eng.vecs ../../bible/enfr/vulic/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/vulic/sgns/eng.vecs ../../bible/enfr/vulic/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/sgns/eng.vecs ../../bible/enes/vulic/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/vulic/sgns/eng.vecs ../../bible/enes/vulic/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enro/vulic/sgns/rom.vecs ../../bible/enro/vulic/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enro/vulic/sgns/rom.vecs ../../bible/enro/vulic/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/vulic/sgns/eng.vecs ../../bible/enfr/vulic/sgns/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/vulic/sgns/eng.vecs ../../bible/enfr/vulic/sgns/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/vulic/sgns/eng.vecs ../../bible/enes/vulic/sgns/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/vulic/sgns/eng.vecs ../../bible/enes/vulic/sgns/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/vulic/sgns/eng.vecs ../../bible/ende/vulic/sgns/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/tfidf/eng.vecs ../../bible/enfr/ppdb/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/tfidf/eng.vecs ../../bible/enfr/ppdb/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/tfidf/eng.vecs ../../bible/enes/ppdb/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/tfidf/eng.vecs ../../bible/enes/ppdb/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/tfidf/eng.vecs ../../bible/enfr/ppdb/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/tfidf/eng.vecs ../../bible/enfr/ppdb/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/tfidf/eng.vecs ../../bible/enes/ppdb/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/tfidf/eng.vecs ../../bible/enes/ppdb/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enro/ppdb/tfidf/rom.vecs ../../bible/enro/ppdb/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enro/ppdb/tfidf/rom.vecs ../../bible/enro/ppdb/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/ppdb/tfidf/eng.vecs ../../bible/enfr/ppdb/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/ppdb/tfidf/eng.vecs ../../bible/enfr/ppdb/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/ppdb/tfidf/eng.vecs ../../bible/enes/ppdb/tfidf/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/ppdb/tfidf/eng.vecs ../../bible/enes/ppdb/tfidf/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/tfidf/eng.vecs ../../bible/ende/ppdb/tfidf/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/pmi/eng.vecs ../../bible/enfr/ppdb/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/pmi/eng.vecs ../../bible/enfr/ppdb/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/pmi/eng.vecs ../../bible/enes/ppdb/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/pmi/eng.vecs ../../bible/enes/ppdb/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/pmi/eng.vecs ../../bible/enfr/ppdb/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/pmi/eng.vecs ../../bible/enfr/ppdb/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/pmi/eng.vecs ../../bible/enes/ppdb/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/pmi/eng.vecs ../../bible/enes/ppdb/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enro/ppdb/pmi/rom.vecs ../../bible/enro/ppdb/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enro/ppdb/pmi/rom.vecs ../../bible/enro/ppdb/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/ppdb/pmi/eng.vecs ../../bible/enfr/ppdb/pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/ppdb/pmi/eng.vecs ../../bible/enfr/ppdb/pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/ppdb/pmi/eng.vecs ../../bible/enes/ppdb/pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/ppdb/pmi/eng.vecs ../../bible/enes/ppdb/pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/pmi/eng.vecs ../../bible/ende/ppdb/pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/sgns/eng.vecs ../../bible/enfr/ppdb/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/sgns/eng.vecs ../../bible/enfr/ppdb/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/sgns/eng.vecs ../../bible/enes/ppdb/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/sgns/eng.vecs ../../bible/enes/ppdb/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/sgns/eng.vecs ../../bible/enfr/ppdb/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/ppdb/sgns/eng.vecs ../../bible/enfr/ppdb/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/sgns/eng.vecs ../../bible/enes/ppdb/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes/ppdb/sgns/eng.vecs ../../bible/enes/ppdb/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enro/ppdb/sgns/rom.vecs ../../bible/enro/ppdb/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enro/ppdb/sgns/rom.vecs ../../bible/enro/ppdb/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/ppdb/sgns/eng.vecs ../../bible/enfr/ppdb/sgns/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/ppdb/sgns/eng.vecs ../../bible/enfr/ppdb/sgns/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/ppdb/sgns/eng.vecs ../../bible/enes/ppdb/sgns/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/ppdb/sgns/eng.vecs ../../bible/enes/ppdb/sgns/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende/ppdb/sgns/eng.vecs ../../bible/ende/ppdb/sgns/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr//tfidf/eng.vecs ../../bible/enfr//tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr//tfidf/eng.vecs ../../bible/enfr//tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes//tfidf/eng.vecs ../../bible/enes//tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes//tfidf/eng.vecs ../../bible/enes//tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr//tfidf/eng.vecs ../../bible/enfr//tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr//tfidf/eng.vecs ../../bible/enfr//tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes//tfidf/eng.vecs ../../bible/enes//tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enes//tfidf/eng.vecs ../../bible/enes//tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enro//tfidf/rom.vecs ../../bible/enro//tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enro//tfidf/rom.vecs ../../bible/enro//tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr//tfidf/eng.vecs ../../bible/enfr//tfidf/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr//tfidf/eng.vecs ../../bible/enfr//tfidf/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes//tfidf/eng.vecs ../../bible/enes//tfidf/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes//tfidf/eng.vecs ../../bible/enes//tfidf/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../bible/ende//tfidf/eng.vecs ../../bible/ende//tfidf/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../bible/enfr//pmi/eng.vecs ../../bible/enfr//pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr//pmi/eng.vecs ../../bible/enfr//pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes//pmi/eng.vecs ../../bible/enes//pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes//pmi/eng.vecs ../../bible/enes//pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr//pmi/eng.vecs ../../bible/enfr//pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr//pmi/eng.vecs ../../bible/enfr//pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes//pmi/eng.vecs ../../bible/enes//pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enes//pmi/eng.vecs ../../bible/enes//pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enro//pmi/rom.vecs ../../bible/enro//pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enro//pmi/rom.vecs ../../bible/enro//pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr//pmi/eng.vecs ../../bible/enfr//pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr//pmi/eng.vecs ../../bible/enfr//pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes//pmi/eng.vecs ../../bible/enes//pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes//pmi/eng.vecs ../../bible/enes//pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../bible/ende//pmi/eng.vecs ../../bible/ende//pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../bible/enfr//sgns/eng.vecs ../../bible/enfr//sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr//sgns/eng.vecs ../../bible/enfr//sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes//sgns/eng.vecs ../../bible/enes//sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes//sgns/eng.vecs ../../bible/enes//sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr//sgns/eng.vecs ../../bible/enfr//sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr//sgns/eng.vecs ../../bible/enfr//sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes//sgns/eng.vecs ../../bible/enes//sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enes//sgns/eng.vecs ../../bible/enes//sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enro//sgns/rom.vecs ../../bible/enro//sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enro//sgns/rom.vecs ../../bible/enro//sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr//sgns/eng.vecs ../../bible/enfr//sgns/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr//sgns/eng.vecs ../../bible/enfr//sgns/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes//sgns/eng.vecs ../../bible/enes//sgns/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes//sgns/eng.vecs ../../bible/enes//sgns/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../bible/ende//sgns/eng.vecs ../../bible/ende//sgns/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../bible/enfr/bilbowa/eng.vecs ../../bible/enfr/bilbowa/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tfr\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enfr/bilbowa/eng.vecs ../../bible/enfr/bilbowa/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tfr\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enes/bilbowa/eng.vecs ../../bible/enes/bilbowa/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\ten\tes\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enes/bilbowa/eng.vecs ../../bible/enes/bilbowa/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "bible\tGOLDEN\tes\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enfr/bilbowa/eng.vecs ../../bible/enfr/bilbowa/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\ten\tfr\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enfr/bilbowa/eng.vecs ../../bible/enfr/bilbowa/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "bible\tHANSARD\tfr\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enes/bilbowa/eng.vecs ../../bible/enes/bilbowa/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\ten\tes\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enes/bilbowa/eng.vecs ../../bible/enes/bilbowa/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "bible\tEPPS\tes\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enro/bilbowa/rom.vecs ../../bible/enro/bilbowa/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "bible\tRADA\ten\tro\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../bible/enro/bilbowa/rom.vecs ../../bible/enro/bilbowa/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "bible\tRADA\tro\ten\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/bilbowa/eng.vecs ../../bible/enfr/bilbowa/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tfr\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enfr/bilbowa/eng.vecs ../../bible/enfr/bilbowa/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tfr\ten\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tde\ten\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/bilbowa/eng.vecs ../../bible/enes/bilbowa/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\ten\tes\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../bible/enes/bilbowa/eng.vecs ../../bible/enes/bilbowa/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "bible\tWIKTIONARY\tes\ten\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-dvd\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_books-music\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-books\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_dvd-music\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-books\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../bible/ende/bilbowa/eng.vecs ../../bible/ende/bilbowa/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "bible\tAMAZON_music-dvd\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/tfidf/eng.vecs ../../euro10k/enfr/vulic/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/tfidf/eng.vecs ../../euro10k/enfr/vulic/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/tfidf/eng.vecs ../../euro10k/enes/vulic/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/tfidf/eng.vecs ../../euro10k/enes/vulic/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/tfidf/eng.vecs ../../euro10k/enfr/vulic/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/tfidf/eng.vecs ../../euro10k/enfr/vulic/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/tfidf/eng.vecs ../../euro10k/enes/vulic/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/tfidf/eng.vecs ../../euro10k/enes/vulic/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/vulic/tfidf/rom.vecs ../../euro10k/enro/vulic/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/vulic/tfidf/rom.vecs ../../euro10k/enro/vulic/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/vulic/tfidf/eng.vecs ../../euro10k/enfr/vulic/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/vulic/tfidf/eng.vecs ../../euro10k/enfr/vulic/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/vulic/tfidf/eng.vecs ../../euro10k/enes/vulic/tfidf/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tvulic\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/vulic/tfidf/eng.vecs ../../euro10k/enes/vulic/tfidf/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/tfidf/eng.vecs ../../euro10k/ende/vulic/tfidf/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tvulic\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/pmi/eng.vecs ../../euro10k/enfr/vulic/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/pmi/eng.vecs ../../euro10k/enfr/vulic/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/pmi/eng.vecs ../../euro10k/enes/vulic/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/pmi/eng.vecs ../../euro10k/enes/vulic/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/pmi/eng.vecs ../../euro10k/enfr/vulic/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/pmi/eng.vecs ../../euro10k/enfr/vulic/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/pmi/eng.vecs ../../euro10k/enes/vulic/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/pmi/eng.vecs ../../euro10k/enes/vulic/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/vulic/pmi/rom.vecs ../../euro10k/enro/vulic/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/vulic/pmi/rom.vecs ../../euro10k/enro/vulic/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/vulic/pmi/eng.vecs ../../euro10k/enfr/vulic/pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/vulic/pmi/eng.vecs ../../euro10k/enfr/vulic/pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/vulic/pmi/eng.vecs ../../euro10k/enes/vulic/pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tvulic\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/vulic/pmi/eng.vecs ../../euro10k/enes/vulic/pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tvulic\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/pmi/eng.vecs ../../euro10k/ende/vulic/pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tvulic\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/sgns/eng.vecs ../../euro10k/enfr/vulic/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/sgns/eng.vecs ../../euro10k/enfr/vulic/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/sgns/eng.vecs ../../euro10k/enes/vulic/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/sgns/eng.vecs ../../euro10k/enes/vulic/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/sgns/eng.vecs ../../euro10k/enfr/vulic/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/vulic/sgns/eng.vecs ../../euro10k/enfr/vulic/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/sgns/eng.vecs ../../euro10k/enes/vulic/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/vulic/sgns/eng.vecs ../../euro10k/enes/vulic/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/vulic/sgns/rom.vecs ../../euro10k/enro/vulic/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/vulic/sgns/rom.vecs ../../euro10k/enro/vulic/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/vulic/sgns/eng.vecs ../../euro10k/enfr/vulic/sgns/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/vulic/sgns/eng.vecs ../../euro10k/enfr/vulic/sgns/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/vulic/sgns/eng.vecs ../../euro10k/enes/vulic/sgns/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tvulic\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/vulic/sgns/eng.vecs ../../euro10k/enes/vulic/sgns/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tvulic\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/vulic/sgns/eng.vecs ../../euro10k/ende/vulic/sgns/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tvulic\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/tfidf/eng.vecs ../../euro10k/enfr/ppdb/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/tfidf/eng.vecs ../../euro10k/enfr/ppdb/tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/tfidf/eng.vecs ../../euro10k/enes/ppdb/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/tfidf/eng.vecs ../../euro10k/enes/ppdb/tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/tfidf/eng.vecs ../../euro10k/enfr/ppdb/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/tfidf/eng.vecs ../../euro10k/enfr/ppdb/tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/tfidf/eng.vecs ../../euro10k/enes/ppdb/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/tfidf/eng.vecs ../../euro10k/enes/ppdb/tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/ppdb/tfidf/rom.vecs ../../euro10k/enro/ppdb/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/ppdb/tfidf/rom.vecs ../../euro10k/enro/ppdb/tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/ppdb/tfidf/eng.vecs ../../euro10k/enfr/ppdb/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/ppdb/tfidf/eng.vecs ../../euro10k/enfr/ppdb/tfidf/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/ppdb/tfidf/eng.vecs ../../euro10k/enes/ppdb/tfidf/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tppdb\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/ppdb/tfidf/eng.vecs ../../euro10k/enes/ppdb/tfidf/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/tfidf/eng.vecs ../../euro10k/ende/ppdb/tfidf/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tppdb\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/pmi/eng.vecs ../../euro10k/enfr/ppdb/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/pmi/eng.vecs ../../euro10k/enfr/ppdb/pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/pmi/eng.vecs ../../euro10k/enes/ppdb/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/pmi/eng.vecs ../../euro10k/enes/ppdb/pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/pmi/eng.vecs ../../euro10k/enfr/ppdb/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/pmi/eng.vecs ../../euro10k/enfr/ppdb/pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/pmi/eng.vecs ../../euro10k/enes/ppdb/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/pmi/eng.vecs ../../euro10k/enes/ppdb/pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/ppdb/pmi/rom.vecs ../../euro10k/enro/ppdb/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/ppdb/pmi/rom.vecs ../../euro10k/enro/ppdb/pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/ppdb/pmi/eng.vecs ../../euro10k/enfr/ppdb/pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/ppdb/pmi/eng.vecs ../../euro10k/enfr/ppdb/pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/ppdb/pmi/eng.vecs ../../euro10k/enes/ppdb/pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tppdb\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/ppdb/pmi/eng.vecs ../../euro10k/enes/ppdb/pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tppdb\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/pmi/eng.vecs ../../euro10k/ende/ppdb/pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tppdb\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/sgns/eng.vecs ../../euro10k/enfr/ppdb/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/sgns/eng.vecs ../../euro10k/enfr/ppdb/sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/sgns/eng.vecs ../../euro10k/enes/ppdb/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/sgns/eng.vecs ../../euro10k/enes/ppdb/sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/sgns/eng.vecs ../../euro10k/enfr/ppdb/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/ppdb/sgns/eng.vecs ../../euro10k/enfr/ppdb/sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/sgns/eng.vecs ../../euro10k/enes/ppdb/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/ppdb/sgns/eng.vecs ../../euro10k/enes/ppdb/sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/ppdb/sgns/rom.vecs ../../euro10k/enro/ppdb/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/ppdb/sgns/rom.vecs ../../euro10k/enro/ppdb/sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/ppdb/sgns/eng.vecs ../../euro10k/enfr/ppdb/sgns/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/ppdb/sgns/eng.vecs ../../euro10k/enfr/ppdb/sgns/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/ppdb/sgns/eng.vecs ../../euro10k/enes/ppdb/sgns/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tppdb\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/ppdb/sgns/eng.vecs ../../euro10k/enes/ppdb/sgns/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tppdb\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/ppdb/sgns/eng.vecs ../../euro10k/ende/ppdb/sgns/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tppdb\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//tfidf/eng.vecs ../../euro10k/enfr//tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//tfidf/eng.vecs ../../euro10k/enfr//tfidf/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//tfidf/eng.vecs ../../euro10k/enes//tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//tfidf/eng.vecs ../../euro10k/enes//tfidf/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//tfidf/eng.vecs ../../euro10k/enfr//tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//tfidf/eng.vecs ../../euro10k/enfr//tfidf/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//tfidf/eng.vecs ../../euro10k/enes//tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//tfidf/eng.vecs ../../euro10k/enes//tfidf/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//tfidf/rom.vecs ../../euro10k/enro//tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//tfidf/rom.vecs ../../euro10k/enro//tfidf/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr//tfidf/eng.vecs ../../euro10k/enfr//tfidf/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr//tfidf/eng.vecs ../../euro10k/enfr//tfidf/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes//tfidf/eng.vecs ../../euro10k/enes//tfidf/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tsid\ttfidf\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes//tfidf/eng.vecs ../../euro10k/enes//tfidf/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tsid\ttfidf\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//tfidf/eng.vecs ../../euro10k/ende//tfidf/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tsid\ttfidf\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//pmi/eng.vecs ../../euro10k/enfr//pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//pmi/eng.vecs ../../euro10k/enfr//pmi/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//pmi/eng.vecs ../../euro10k/enes//pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//pmi/eng.vecs ../../euro10k/enes//pmi/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//pmi/eng.vecs ../../euro10k/enfr//pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//pmi/eng.vecs ../../euro10k/enfr//pmi/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//pmi/eng.vecs ../../euro10k/enes//pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//pmi/eng.vecs ../../euro10k/enes//pmi/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//pmi/rom.vecs ../../euro10k/enro//pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//pmi/rom.vecs ../../euro10k/enro//pmi/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr//pmi/eng.vecs ../../euro10k/enfr//pmi/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr//pmi/eng.vecs ../../euro10k/enfr//pmi/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes//pmi/eng.vecs ../../euro10k/enes//pmi/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tsid\tpmi\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes//pmi/eng.vecs ../../euro10k/enes//pmi/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tsid\tpmi\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//pmi/eng.vecs ../../euro10k/ende//pmi/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tsid\tpmi\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//sgns/eng.vecs ../../euro10k/enfr//sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//sgns/eng.vecs ../../euro10k/enfr//sgns/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//sgns/eng.vecs ../../euro10k/enes//sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//sgns/eng.vecs ../../euro10k/enes//sgns/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//sgns/eng.vecs ../../euro10k/enfr//sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr//sgns/eng.vecs ../../euro10k/enfr//sgns/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//sgns/eng.vecs ../../euro10k/enes//sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enes//sgns/eng.vecs ../../euro10k/enes//sgns/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//sgns/rom.vecs ../../euro10k/enro//sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enro//sgns/rom.vecs ../../euro10k/enro//sgns/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr//sgns/eng.vecs ../../euro10k/enfr//sgns/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr//sgns/eng.vecs ../../euro10k/enfr//sgns/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes//sgns/eng.vecs ../../euro10k/enes//sgns/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tsid\tsgns\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes//sgns/eng.vecs ../../euro10k/enes//sgns/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tsid\tsgns\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende//sgns/eng.vecs ../../euro10k/ende//sgns/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tsid\tsgns\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/bilbowa/eng.vecs ../../euro10k/enfr/bilbowa/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tfr\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/bilbowa/eng.vecs ../../euro10k/enfr/bilbowa/fre.vecs ../data/golden_collection/en-fr_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.fr R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tfr\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/bilbowa/eng.vecs ../../euro10k/enes/bilbowa/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\ten\tes\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/bilbowa/eng.vecs ../../euro10k/enes/bilbowa/spa.vecs ../data/golden_collection/en-es_1-100.wa ../data/golden_collection/sentences/1-100-final.en ../data/golden_collection/sentences/1-100-final.es R | tr -d '[[:space:]]'`
echo -e "euro10k\tGOLDEN\tes\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/bilbowa/eng.vecs ../../euro10k/enfr/bilbowa/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\ten\tfr\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enfr/bilbowa/eng.vecs ../../euro10k/enfr/bilbowa/fre.vecs ../data/English-French/answers/test.wa.nonullalign ../data/English-French/test/test.e ../data/English-French/test/test.f R | tr -d '[[:space:]]'`
echo -e "euro10k\tHANSARD\tfr\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/bilbowa/eng.vecs ../../euro10k/enes/bilbowa/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\ten\tes\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enes/bilbowa/eng.vecs ../../euro10k/enes/bilbowa/spa.vecs ../data/epps-enes-alignref.v2005-12-05/test/test.engspa.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.eng.iso.naacl ../data/epps-enes-alignref.v2005-12-05/test/test.spa.iso.naacl R | tr -d '[[:space:]]'`
echo -e "euro10k\tEPPS\tes\ten\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/bilbowa/rom.vecs ../../euro10k/enro/bilbowa/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e R | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\ten\tro\tN/A\tbilbowa\t$result"
result=`python omer-align-fast.py ../../euro10k/enro/bilbowa/rom.vecs ../../euro10k/enro/bilbowa/eng.vecs ../data/Romanian-English/answers/test.wa.nonullalign ../data/Romanian-English/test/test.r ../data/Romanian-English/test/test.e | tr -d '[[:space:]]'`
echo -e "euro10k\tRADA\tro\ten\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/bilbowa/eng.vecs ../../euro10k/enfr/bilbowa/fre.vecs ../data/en-fr-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tfr\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enfr/bilbowa/eng.vecs ../../euro10k/enfr/bilbowa/fre.vecs ../data/en-fr-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tfr\ten\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-de-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-de-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tde\ten\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/bilbowa/eng.vecs ../../euro10k/enes/bilbowa/spa.vecs ../data/en-es-enwiktionary.txt | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\ten\tes\tN/A\tbilbowa\t$result"
result=`python omer-wiktionary-fast.py ../../euro10k/enes/bilbowa/eng.vecs ../../euro10k/enes/bilbowa/spa.vecs ../data/en-es-enwiktionary.txt R | tr -d '[[:space:]]'`
echo -e "euro10k\tWIKTIONARY\tes\ten\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-books-train/ ../data/en-dvd-train/ ../data/de-books-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-dvd\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-books-train/ ../data/en-music-train/ ../data/de-books-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_books-music\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-dvd-train/ ../data/en-books-train/ ../data/de-dvd-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-books\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-dvd-train/ ../data/en-music-train/ ../data/de-dvd-test/ ../data/de-music-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_dvd-music\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-music-train/ ../data/en-books-train/ ../data/de-music-test/ ../data/de-books-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-books\ten\tde\tN/A\tbilbowa\t$result"
result=`python omer-topic-fast.py ../../euro10k/ende/bilbowa/eng.vecs ../../euro10k/ende/bilbowa/deu.vecs ../data/en-music-train/ ../data/en-dvd-train/ ../data/de-music-test/ ../data/de-dvd-test/ | tr -d '[[:space:]]'`
echo -e "euro10k\tAMAZON_music-dvd\ten\tde\tN/A\tbilbowa\t$result"

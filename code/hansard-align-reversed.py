from __future__ import division
import sys,re
from scipy import stats
from scipy.spatial.distance import euclidean as Ed
import numpy as np
import glob

#todo: filter for stopwords

E=[(l.strip().split()[0].split("_")[0],l.strip().split()[1:]) for l in open(sys.argv[1]).readlines() if not re.compile("^\s*$").match(l)] # merged embeddings file
E=dict(E)

if sys.argv[-1]=="S":
    aligns=[[int(x) for x in l.strip().split()[:3]] for l in open(sys.argv[2]).readlines() if l.strip().split()[-1]!="P"]
else:
    aligns=[[int(x) for x in l.strip().split()[:3]] for l in open(sys.argv[2]).readlines()]

ssents=[l.strip().split(">")[1].split("<")[0].split() for l in open(sys.argv[4]).readlines()]
tsents=[l.strip().split(">")[1].split("<")[0].split() for l in open(sys.argv[3]).readlines()] #swap 3 and 4

tot,score=0,0
scov,tcov=0,0

sc=1 # sentence counter
for a in aligns:
    sid,twid,swid=a # replace swid and twid
#    print ssents, sid, swid, twid
    sword=ssents[sid-1][swid-1]
    tword=tsents[sid-1][twid-1]
    
    twords=tsents[sid-1]
    if sword in E:
        scov+=1
    if tword in E:
        tcov+=1

    if sword in E and tword in E:
        tot+=1
        cand=""
        mini=1000000000000000000000
        for t in twords:
            if t in E:
                A=[float(x) for x in E[sword]]
                B=[float(x) for x in E[t]]
                dist=Ed(np.array(A),np.array(B))
                if dist<mini: 
                    mini=dist
                    cand=t
        if cand==tword:
            score+=1

print score/tot, "(", score, "/", tot, ")",
print "coverage:", tot/len(aligns), scov/len(aligns), tcov/len(aligns)
